﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.IO;
using System.Windows.Threading;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using TwitchLib.Client.Enums;
using TwitchLib.Client.Events;

namespace AlphaTwitchBot
{
    public partial class MainWindow : Window
    {
        DispatcherTimer _mainTimer = new DispatcherTimer();

        private const string botUserName = "JiggAlphaBot", 
                             botPassword = "ksoLewNb94668";

        TwitchConnector connector;

        private ObservableCollection<Chatter> _chatters;
        public ObservableCollection<Chatter> Chatters
        {
            get { return _chatters; }
            set
            {
                if(value != null)
                {
                    _chatters = value;
                }
            }
        }

        private ObservableCollection<Chatter> _modChatters;
        public ObservableCollection<Chatter> ModChatters
        {
            get { return _modChatters; }
            set
            {
                if (value != null)
                {
                    _modChatters = value;
                }
            }
        }

        public bool IsConnectBtnEnabled
        {
            get; private set;
        } = false;
        public bool IsDisconnectBtnEnabled
        {
            get; private set;
        } = false;

        private CommandsFactory _commandFactory;

        public MainWindow()
        {
            InitializeComponent();
            _commandFactory = new CommandsFactory(SongRequesterControl, CommandsControl);
            connector = new TwitchConnector(_commandFactory);
            connector.ReceiveChatMessage += Connector_ReceiveChatMessage;
            connector.ReceivedWhisperMessage += Connector_ReceivedWhisperMessage;
            DataContext = this;
            _chatters = new ObservableCollection<Chatter>();
            _modChatters = new ObservableCollection<Chatter>();
            connector.ChattersList.CollectionChanged += UserList_CollectionChanged;
            connector.ModChattersList.CollectionChanged += UserList_CollectionChanged;
            SongRequesterControl.SRAddSongConfirmation += SongRequesterControl_SRAddSongConfirmation;
            CommandsControl.ShowCommandResponse += CommandsControl_ShowCommandResponse;

        }

        private void CommandsControl_ShowCommandResponse(object sender, string e)
        {
            connector?.SendChatMessage(e);
        }

        private void SongRequesterControl_SRAddSongConfirmation(object sender, string e)
        {
            connector?.SendChatMessage(e);
        }

        private void UserList_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
                {
                    var newChatter = (Chatter)e.NewItems[0];
                    if(newChatter.UserType == UserType.Moderator)
                    {
                        if (!_modChatters.Any(x => x.Username == newChatter.Username))
                        {
                            AddUserToListInOrder(newChatter, _modChatters);
                            RaisePropertyChanged(nameof(ModChatters));
                        }
                            
                    }
                    else
                    {
                        if (!_chatters.Any(x => x.Username == newChatter.Username))
                        {
                            AddUserToListInOrder(newChatter, _chatters);
                            RaisePropertyChanged(nameof(_chatters));
                        }
                    }
                    
                }
                else if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
                {
                    var oldChatter = (Chatter)e.OldItems[0];
                    if (oldChatter.UserType == UserType.Moderator)
                    {
                        if (_modChatters.Any(x => x.Username == oldChatter.Username))
                        {
                            _modChatters.Remove(oldChatter);
                            RaisePropertyChanged(nameof(ModChatters));
                        }
                    }
                    else
                    {
                        if (_chatters.Any(x => x.Username == oldChatter.Username))
                        {
                            _chatters.Remove(oldChatter);
                            RaisePropertyChanged(nameof(Chatters));
                        }
                    }
                }
            });
        }

        private void AddUserToListInOrder(Chatter newUser, ObservableCollection<Chatter> userList)
        {
            if (userList.Count > 1)
            {
                bool added = false;
                foreach (Chatter chatter in userList)
                {
                    int result = chatter.Username.CompareTo(newUser.Username);
                    if(result == 1)
                    {
                        userList.Insert(userList.IndexOf(chatter), newUser);
                        added = true;
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }
                if(!added)
                    userList.Add(newUser);
            }
            else
            {
                userList.Add(newUser);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private void Connector_ReceiveChatMessage(object sender, OnMessageReceivedArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                //check first if its a command
                if (e.ChatMessage.Message.StartsWith("!"))
                    _commandFactory.ProcessCommand(e.ChatMessage);

                MainChat.AppendText(e.ChatMessage.Username + " : " + e.ChatMessage.Message + Environment.NewLine);
                MainChat.ScrollToEnd();
            });
        }

        private void Connector_ReceivedWhisperMessage(object sender, OnWhisperReceivedArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                WhispersChat.AppendText(e.WhisperMessage.Username + " : " + e.WhisperMessage.Message + Environment.NewLine);
                WhispersChat.ScrollToEnd();
            });
        }

        private void SendMessageBtnClick(object sender, RoutedEventArgs e)
        {
            SendChatMessage();
        }

        private void Button_KeyDown(object sender, KeyEventArgs e)
        {
            SendChatMessage();
        }

        private void SendChatMessage()
        {
            if (MainTextBox.Text != null && MainTextBox.Text != string.Empty)
            {
                connector?.SendChatMessage(MainTextBox.Text);
                MainChat.AppendText(botUserName + " : " + MainTextBox.Text + Environment.NewLine);
                MainTextBox.Clear();
                MainChat.ScrollToEnd();
            }
        }
        
        private void SendMultipleWhisperClick(object sender, RoutedEventArgs e)
        {
            ShowWhisperMessageDialog();
        }

        private void ShowWhisperMessageDialog()
        {
            WhisperDialog inputDialog = new WhisperDialog("Please enter your message:", "");
            if (inputDialog.ShowDialog() == true)
            {
                var message = inputDialog.Answer;
                var users = UsersListBox.SelectedItems;

                foreach (Chatter user in users)
                {
                    connector.SendPrivateWhisper(user.Username, message);
                }
            } 
        }

        private void SendSingleWhisperClick(object sender, RoutedEventArgs e)
        {
            var source = e.OriginalSource;
        }
        
        private void MainTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                SendChatMessage();
            }
        }

        private void SelectWhisperingUsers_Drop(object sender, DragEventArgs e)
        {
            var sresult = (ListBoxItem)e.Data.GetData(typeof(ListBoxItem));
            var result = (Chatter)sresult.Content;
            if (result != null)
            {
                SelectWhisperingUsers.Items.Add(result);
            }
        }
        
        private void UsersListBox_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var item = ItemsControl.ContainerFromElement(sender as ListBox, e.OriginalSource as DependencyObject) as ListBoxItem;
            if (item != null)
            {
                DragDrop.DoDragDrop(UsersListBox, item, DragDropEffects.Move);
            }
        }

        private void SelectWhisperingUsers_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                SelectWhisperingUsers.Items.Remove(SelectWhisperingUsers.SelectedItem);
            }
        }
        
        private void SendMessageToWhisperUsers_Click(object sender, RoutedEventArgs e)
        {
            if (SelectWhisperingUsers.Items.Count > 0 &&
                        WhisperTextBox.Text != string.Empty )
            {
                var message = WhisperTextBox.Text;
                foreach (Chatter user in SelectWhisperingUsers.Items)
                {
                    
                    Thread.Sleep(1500);
                    connector.SendPrivateWhisper(user.Username, message);
                    message = message + " ";
                }

                MessageBox.Show("Multiple whispers sent!");
            }
            else
            {
                MessageBox.Show("No users in the list!");
            }
        }

        private void ModsListBox_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var item = ItemsControl.ContainerFromElement(sender as ListBox, e.OriginalSource as DependencyObject) as ListBoxItem;
            if (item != null)
            {
                DragDrop.DoDragDrop(UsersListBox, item, DragDropEffects.Move);
            }
        }
        
        private void UsernameFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            ICollectionView view = CollectionViewSource.GetDefaultView(UsersListBox.ItemsSource);
            view.Filter = o => (((Chatter)o).Username.Contains(UsernameFilter.Text));
        }

        private void ClearWhisperListUsersButton_Click(object sender, RoutedEventArgs e)
        {
            SelectWhisperingUsers.Items.Clear();
        }

        public void ReceiveMessage(string speaker, string receivedMessage)
        {
            MainChat.AppendText(speaker + " : " + receivedMessage + Environment.NewLine);
            MainChat.ScrollToEnd();
        }
        
    }
    
}
