﻿using AlphaTwitchBot.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Controls;

namespace AlphaTwitchBot.Controls
{

    public partial class BotCommandsControl : UserControl
    {
        private readonly string commandsFilePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "botcommands.json");

        public List<BotCommand> BotCommands
        {
            get;
            set;
        }
        
        public BotCommandsControl()
        {
            InitializeComponent();
            this.DataContext = this;
            BotCommands = new List<BotCommand>();
            LoadCommandsFile();
        }
        
        public void LoadCommandsFile()
        {
            if (File.Exists(commandsFilePath))
            {
                BotCommands = JsonConvert.DeserializeObject<List<BotCommand>>(File.
                                                                        ReadAllText(commandsFilePath));
            }
        }
        
        private void CommandsGrid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            SaveCommandsToFile();
        }

        private void SaveCommandsToFile()
        {
            using (StreamWriter writer = new StreamWriter(commandsFilePath))
            {
                writer.Write(JsonConvert.SerializeObject(BotCommands));
            }
        }

        public event EventHandler<string> ShowCommandResponse;

        public void HandleCommandResponse(string commandResponse)
        {
            ShowCommandResponse.Invoke(this, commandResponse);
        }
        
    }
}
