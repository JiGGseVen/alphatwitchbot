﻿using SpotifyAPI.Local;
using SpotifyAPI.Web;
using SpotifyAPI.Web.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AlphaTwitchBot.Controls
{
    public partial class SpotifySongRequestControl : UserControl, INotifyPropertyChanged
    {

        private SpotifyLocalAPI _spotifyLocal;
        private SpotifyWebAPI _spotifyApi;

        private FullTrack CurrentTrack { get; set; }

        private int CurrentTrackIndex;

        public bool SRStatus { get; set; } = false;

        public Visibility SongsOnPlaylist
        {
            get
            {
                if(TracksList == null || TracksList.Count <= 0)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
        }

        private string _currentSong;
        public string CurrentSong
        {
            get
            {
                return _currentSong;
            }
            set
            {
                _currentSong = value;
            }
        }

        private ObservableCollection<FullTrack> _tracksList;

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<FullTrack> TracksList
        {
            get
            {
                return _tracksList;
            }
            set
            {
                _tracksList = value;
            }
        }
        public SpotifySongRequestControl()
        {
            InitializeComponent();

            _spotifyApi = new SpotifyWebAPI();
            _spotifyLocal = new SpotifyLocalAPI();
            _spotifyLocal.Connect();
            _spotifyLocal.OnPlayStateChange += _spotifyLocal_OnPlayStateChange;

            _spotifyApi.UseAuth = false;
            _tracksList = new ObservableCollection<FullTrack>();
            CurrentTrackIndex = 0;

            TracksList.CollectionChanged += TracksList_CollectionChanged;
            this.DataContext = this;
        }

        private void TracksList_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            RaisePropertyChanged(nameof(TracksList));
        }
        
        protected void RaisePropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private void _spotifyLocal_OnPlayStateChange(object sender, PlayStateEventArgs e)
        {
            
        }

        public bool HandleSongRequest(string searchString)
        {
            var result = SearchSong(searchString);

            if(result != null)
            {
                AddSongToPlaylist(result);
                return true;
            }
            else
                return false;
        }

        public FullTrack SearchSong(string track)
        {
            var result = _spotifyApi.SearchItems(track, SpotifyAPI.Web.Enums.SearchType.All);
            
            FullTrack resultTrack = result.Tracks.Items.First();

            if (resultTrack != null)
                return resultTrack;
            else
                return null;
        }

        public void PlayTrack(string trackUri)
        {
            _spotifyLocal.PlayURL(trackUri);
        }

        public void PlayNext()
        {
            _spotifyLocal.Skip();
        }

        private void AddSongToPlaylist(FullTrack track)
        {
            TracksList.Add(track);
            SRAddSongConfirmation.Invoke(this,"The song " + track.Name + " " + track.Artists[0]?.Name + " was added to the song request queue!");
            //await _spotifyLocal.AddToQueue(track.Uri);
            //await _spotifyLocal.PlayURL(track.Uri);
        }

        private void RemoveSongFromPlaylist(FullTrack track)
        {
            TracksList.Remove(track);
        }

        private void RemoveSongFromPlaylist(int trackIndex)
        {
            TracksList.RemoveAt(trackIndex);
        }

        private void PauseButton_Click(object sender, RoutedEventArgs e)
        {
            _spotifyLocal.Pause();
        }

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            _spotifyLocal.PlayURL(TracksList[CurrentTrackIndex].Uri);
            CurrentTrack = TracksList[CurrentTrackIndex];
        }

        private void Backward_Click(object sender, RoutedEventArgs e)
        {
            var previousTrackIndex = CurrentTrackIndex - 1;
            if (previousTrackIndex >= 0 )
            {
                _spotifyLocal.PlayURL(TracksList[previousTrackIndex]?.Uri);
                CurrentTrackIndex = previousTrackIndex;
                CurrentTrack = TracksList[previousTrackIndex];
            }
        }

        private void Forward_Click(object sender, RoutedEventArgs e)
        {
            var nextTrackIndex = CurrentTrackIndex + 1;
            if (nextTrackIndex >= 0 || nextTrackIndex < TracksList.Count)
            {
                _spotifyLocal.PlayURL(TracksList[nextTrackIndex]?.Uri);
                CurrentTrackIndex = nextTrackIndex;
                CurrentTrack = TracksList[nextTrackIndex];
            }
        }

        public void TurOnSongRequest()
        {
            SRStatus = true;
        }

        public void TurnOffSongrequest()
        {
            SRStatus = false;
        }
        
        public event EventHandler<string> SRAddSongConfirmation;

        private void ToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            SRStatus = true;
        }

        private void ToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            SRStatus = false;
        }
    }
}
