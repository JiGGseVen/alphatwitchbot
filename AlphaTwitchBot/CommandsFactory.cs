﻿using AlphaTwitchBot.Controls;
using AlphaTwitchBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchLib.Client.Models;

namespace AlphaTwitchBot
{
    public class CommandsFactory
    {
        //Here all the commands command from the chat will be processed
        //song request commands
        private readonly string songRequestCommand = "!sr";
        private readonly string skipSongCommand = "!srskip";
        private readonly string previousSongCommand = "!srprevious";
        private readonly string removeSongRequestCommand = "!srremove";
        private readonly string songRequestOnCommand = "!sron";
        private readonly string songRequestOffCommand = "!sroff";
        private readonly string songRequestUpCommand = "!srvolumeup";
        private readonly string songRequestDownCommand = "!srvolumedown";

        private Dictionary<string, Func<string, bool>> commandsDictionary;

        private SpotifySongRequestControl _songRequester;
        private BotCommandsControl _commandsControl;

        public CommandsFactory(SpotifySongRequestControl songRequesterControl, BotCommandsControl commandsControl)
        {
            commandsDictionary = new Dictionary<string, Func<string, bool>>()
            {
                { songRequestCommand, ProcessSongRequestAdd },
                { skipSongCommand, ProcessSkipCommand },
                { previousSongCommand, ProcessPreviousSrCommand },
                { removeSongRequestCommand, ProcessRemoveSrCommand },
                { songRequestOnCommand, ProcessSrOnCommand },
                { songRequestOffCommand, ProcessSrOffommand },
                { songRequestUpCommand, ProcessSrUpCommand },
                { songRequestDownCommand, ProcessSrDownCommand }
            };
            
            _songRequester = songRequesterControl;
            _commandsControl = commandsControl;
        }
        
        private bool ProcessSrDownCommand(string arg)
        {
            throw new NotImplementedException();
        }

        private bool ProcessSrUpCommand(string arg)
        {
            throw new NotImplementedException();
        }

        private bool ProcessSrOffommand(string arg)
        {
            throw new NotImplementedException();
        }

        private bool ProcessSrOnCommand(string arg)
        {
            throw new NotImplementedException();
        }

        private bool ProcessRemoveSrCommand(string arg)
        {
            throw new NotImplementedException();
        }

        private bool ProcessPreviousSrCommand(string arg)
        {
            throw new NotImplementedException();
        }

        private bool ProcessSkipCommand(string arg)
        {
            throw new NotImplementedException();
        }

        private bool ProcessSongRequestAdd(string message)
        {
            var result = _songRequester.HandleSongRequest(message);
            return result;
        }

        public void ProcessCommand(ChatMessage message)
        {
            var result = message.Message.ToLower();
            int initIndex = result.IndexOf(' ');
            if(initIndex >= 0)//with arguments
            {
                var command = result.Substring(0, initIndex);
                var argument = result.Substring(initIndex + 1);

                if (command.Equals("!followage"))
                {
                    CheckUserFollowage(message, command);
                }

                //if (command.StartsWith("!sr"))
                //{
                //    ParseCommand(command, argument);
                //}
                
                //handle all other commands with arguments
            }
            else//without arguments
            {
                var command = _commandsControl.BotCommands.Find(x => x.CommandCode == result);
                if (command != null)
                {
                    _commandsControl.HandleCommandResponse(command.CommandResponse);
                }
            }
            
        }

        public void ParseCommand(string command, string argument)
        {
            if (commandsDictionary.ContainsKey(command))
            {
                var value = commandsDictionary[command];
                value.Invoke(argument);
            }
        }

        public void CheckUserFollowage(ChatMessage message, string argument)
        {

            _commandsControl.HandleCommandResponse("");
        }

    }
}
