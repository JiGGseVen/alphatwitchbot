﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Diagnostics;
using System.Windows;
using System.Threading;
using TwitchLib.Client;
using TwitchLib.Client.Extensions;
using TwitchLib.Client.Models;
using TwitchLib.Api;
using TwitchLib.Api.Interfaces;
using TwitchLib.Api.Core.Models.Undocumented.Chatters;
using System;
using TwitchLib.Api.V5.Models.Subscriptions;
using TwitchLib.Client;
using TwitchLib.Client.Enums;
using TwitchLib.Client.Events;
using TwitchLib.Client.Extensions;
using TwitchLib.Client.Models;

namespace AlphaTwitchBot
{
    public class TwitchConnector
    {
        private string TwitchUsername = "JiggAlphaBot";
        private string TwitchAuth = "oauth:azqae45o01ytw125ke8tpw7lv7uspt";

        private string TwitchChannel = "nouschka";
        private string ClientID = "1e9ayharyd70aypqvzyt8sckif0ie2";
        private string TSecret = "r6uxcfcsu4p3iyd7vwil1yiu2x5w13";

        private string NouChannelID = "77009553";

        public string CurrentJoinedChannel;

        private static TwitchAPI api;

        private ObservableCollection<Chatter> _chattersList;
        public ObservableCollection<Chatter> ChattersList
        {
            get
            {
                return _chattersList;
            }
            set
            {
                if (value != null)
                    _chattersList = value;
            }
        }

        private ObservableCollection<Chatter> _modChattersList;
        public ObservableCollection<Chatter> ModChattersList
        {
            get
            {
                return _modChattersList;
            }
            set
            {
                if (value != null)
                    _modChattersList = value;
            }
        }

        public bool IsClientConnect
        {
            get { return client.IsConnected; }
        }

        public event EventHandler<OnMessageReceivedArgs> ReceiveChatMessage;
        public event EventHandler<OnWhisperReceivedArgs> ReceivedWhisperMessage;

        private CommandsFactory _commandFactory;

        public TwitchClient client;

        public TwitchConnector(CommandsFactory commandFactory)
        {
            _commandFactory = commandFactory;
            SetupConnection();
        }

        private void SetupConnection()
        {
            ConnectionCredentials credentials = new ConnectionCredentials(TwitchUsername, TwitchAuth);
            client = new TwitchClient();
            client.Initialize(credentials, TwitchChannel);


            _chattersList = new ObservableCollection<Chatter>();
            _modChattersList = new ObservableCollection<Chatter>();
            
            InitializeEvents();
            client.Connect();

            SetupTwitchApi();
        }

        private void SetupTwitchApi()
        {
            api = new TwitchAPI();
            api.Settings.ClientId = ClientID;
            //api.Settings.AccessToken = TSecret;
        }

        void InitializeEvents()
        {
            client.OnConnected += Client_OnConnected;
            client.OnLog += Client_OnLog;
            client.OnJoinedChannel += Client_OnJoinedChannel;
            client.OnMessageReceived += Client_OnMessageReceived;
            client.OnWhisperReceived += Client_OnWhisperReceived;
            client.OnNewSubscriber += Client_OnNewSubscriber;
            client.OnUserJoined += Client_OnUserJoined;
            client.OnUserLeft += Client_OnUserLeft;
        }

        private void Client_OnConnected(object sender, OnConnectedArgs e)
        {
            var channelName = e.AutoJoinChannel;
        }

        private void Client_OnLog(object sender, OnLogArgs e)
        {
            //TODO
        }

        private void Client_OnJoinedChannel(object sender, OnJoinedChannelArgs e)
        {
            CurrentJoinedChannel = e.Channel;
            GetChannelChatters(); //works for mods and viewers
        }

        private void Client_OnMessageReceived(object sender, OnMessageReceivedArgs e)
        {
            if (e.ChatMessage.Message.Equals("!followage"))
                CheckUserFollowage(e.ChatMessage);
            
            ReceiveChatMessage?.Invoke(sender, e);
        }

        private async void CheckUserFollowage(ChatMessage message)
        {
            var userId = message.UserId;
            var userFollow = await api.V5.Users.CheckUserFollowsByChannelAsync(userId, NouChannelID);
            var duration = DateTime.Now - userFollow.CreatedAt;

            int years, months, days, hours, minutes, seconds, miliseconds = 0;
            GetElapsedTime(userFollow.CreatedAt, DateTime.Now, out years, out months, out days, out hours, out minutes, out seconds, out miliseconds);

            var finalMessage = "User " + message.Username + " has been following the DESTROYER for: " + years +
                             " year(s), " + months + " month(s), " + days + " day(s) and " + hours + " hours! Whoop Whoop!";
            SendChatMessage(finalMessage);
        }

        private void Client_OnWhisperReceived(object sender, OnWhisperReceivedArgs e)
        {
            ReceivedWhisperMessage?.Invoke(sender, e);
        }

        private void Client_OnNewSubscriber(object sender, OnNewSubscriberArgs e)
        {
            //TODO
        }

        private void RetrieveChannelMods()
        {
            client.GetChannelModerators(TwitchChannel);
        }
        
        private void Client_OnUserJoined(object sender, OnUserJoinedArgs e)
        {
            var username = e.Username;
            var users = _chattersList.Where(x => x.Username == username);
            if (!users.Any()) return;

            var newUser = new Chatter(username, UserType.Viewer);
            AddSortedUser(newUser);
        }

        private void AddSortedUser(Chatter newUser)
        {
            var username = newUser.Username;

            foreach (Chatter item in _chattersList)
            {
                var result = string.Compare(item.Username, username);
                if (result != 1)
                    continue;
                else
                {
                    _chattersList.Insert(_chattersList.IndexOf(item), newUser);
                    break;
                }
            }
        }
        
        private void Client_OnUserLeft(object sender, OnUserLeftArgs e)
        {
            var username = e.Username;
            if(_chattersList.Any())
                _chattersList.Remove(_chattersList.Where(x => x.Username == username)?.First());
        }
        
        public async void GetChannelChatters()
        {
            List<Chatter> chattersList = new List<Chatter>();
            var uL = await api.V5.Users.GetUserByNameAsync("mrjiggs");
            var uL2 = await api.V5.Users.GetUserByNameAsync("nouschka");

            var channelInfo = await api.V5.Channels.GetChannelByIDAsync(uL.Matches.First().Id);
            //var channelFollowers = await api.V5.Channels.GetChannelFollowersAsync(NouChannelID);
            
            var userFollowage2 = await api.V5.Users.CheckUserFollowsByChannelAsync(uL.Matches.First().Id, NouChannelID);

            
            var usersInChat = await api.Undocumented.GetChattersAsync(TwitchChannel);

            foreach (ChatterFormatted chatter in usersInChat)
            {
                if(chatter.UserType == TwitchLib.Api.Core.Enums.UserType.Moderator)
                {
                    _modChattersList.Add(new Chatter(chatter.Username, UserType.Moderator));
                }
                else
                {
                    _chattersList.Add(new Chatter(chatter.Username, UserType.Viewer));
                }
            }
        }
        
        public void SendChatMessage(string message)
        {
            client.SendMessage(TwitchChannel, message);
        }

        
        public void BotJoinChannel(string channelName)
        {
            client?.JoinChannel(channelName);
        }
    
        public void BotLeaveChannel(string channelName)
        {
            client?.LeaveChannel(channelName);
        }

        public void SendPrivateWhisper(string username, string message)
        {
            client.SendWhisper(username, message);
        }

        private async void GetListOfChannelSubs()
        {
            List<Subscription> allSubscriptions = await api.V5.Channels.GetAllSubscribersAsync("channel_id");
        }
        public static void TimeSpanToDateParts(DateTime d1, DateTime d2, out int years, out int months, out int days, out int hours, out int minutes)
        {
            if (d1 < d2)
            {
                var d3 = d2;
                d2 = d1;
                d1 = d3;
            }

            var span = d1 - d2;

            months = 12 * (d1.Year - d2.Year) + (d1.Month - d2.Month);

            //month may need to be decremented because the above calculates the ceiling of the months, not the floor.
            //to do so we increase d2 by the same number of months and compare.
            //(500ms fudge factor because datetimes are not precise enough to compare exactly)
            if (d1.CompareTo(d2.AddMonths(months).AddMilliseconds(-500)) <= 0)
            {
                --months;
            }

            years = months / 12;
            months -= years * 12;

            if (months == 0 && years == 0)
            {
                days = span.Days;
            }
            else
            {
                var md1 = new DateTime(d1.Year, d1.Month, d1.Day);
                // Fixed to use d2.Day instead of d1.Day
                var md2 = new DateTime(d2.Year, d2.Month, d2.Day);
                var mDays = (int)(md1 - md2).TotalDays;

                if (mDays > span.Days)
                {
                    mDays = (int)(md1.AddMonths(-1) - md2).TotalDays;
                }

                days = span.Days - mDays;


            }
            hours = span.Hours;
            minutes = span.Minutes;
        }

        // Return the number of years, months, days, hours,
        // minutes, seconds, and milliseconds you need to add to
        // from_date to get to_date.
        private void GetElapsedTime(DateTime from_date, DateTime to_date,
            out int years, out int months, out int days, out int hours,
            out int minutes, out int seconds, out int milliseconds)
        {
            // If from_date > to_date, switch them around.
            if (from_date > to_date)
            {
                GetElapsedTime(to_date, from_date,
                    out years, out months, out days, out hours,
                    out minutes, out seconds, out milliseconds);
                years = -years;
                months = -months;
                days = -days;
                hours = -hours;
                minutes = -minutes;
                seconds = -seconds;
                milliseconds = -milliseconds;
            }
            else
            {
                // Handle the years.
                years = to_date.Year - from_date.Year;

                // See if we went too far.
                DateTime test_date = from_date.AddMonths(12 * years);
                if (test_date > to_date)
                {
                    years--;
                    test_date = from_date.AddMonths(12 * years);
                }

                // Add months until we go too far.
                months = 0;
                while (test_date <= to_date)
                {
                    months++;
                    test_date = from_date.AddMonths(12 * years + months);
                }
                months--;

                // Subtract to see how many more days,
                // hours, minutes, etc. we need.
                from_date = from_date.AddMonths(12 * years + months);
                TimeSpan remainder = to_date - from_date;
                days = remainder.Days;
                hours = remainder.Hours;
                minutes = remainder.Minutes;
                seconds = remainder.Seconds;
                milliseconds = remainder.Milliseconds;
            }
        }
    }

    public class Chatter
    {
        public Chatter(string username, UserType userType)
        {
            Username = username;
            UserType = userType;
        }

        public string Username { get; set; }
        public UserType UserType { get; set; }
    }
}
