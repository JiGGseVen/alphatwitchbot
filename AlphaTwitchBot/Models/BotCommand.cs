﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaTwitchBot.Models
{
    public class BotCommand
    {
        public string CommandCode { get; set; }
        public string CommandResponse { get; set; }

    }
}
