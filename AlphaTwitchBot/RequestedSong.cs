﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaTwitchBot
{
    class RequestedSong
    {
        public int SongID
        {
            get; set;
        }

        public string SongUri
        {
            get; set;
        }

        public string SongName
        {
            get; set;
        }

        public string Requester
        {
            get; set;
        }
    }
}
