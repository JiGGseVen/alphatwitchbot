﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.IO;
using System.Windows.Threading;

namespace AlphaTwitchBot
{
    public partial class MainWindow : Window
    {
        DispatcherTimer _mainTimer = new DispatcherTimer();
        private readonly string selfpasswd = "oauth:azqae45o01ytw125ke8tpw7lv7uspt";

        int i = 0;
        TcpClient tcpClient;
        StreamReader twitchReader;
        StreamWriter twitchWriter;

        private const string botUserName = "jiggialphabot", botPassword = "ksoLewNb94668";
        private string chatRoomName;
        private const string chatCommand = "PRIVMSG";
        private string chatPrefix;

        public MainWindow()
        {
            InitializeComponent();
            InitializeBOT();
            chatRoomName = string.Empty;
        }

        private void SetupAndStartTimer()
        {
            _mainTimer.Interval = TimeSpan.FromMilliseconds(1000);
            _mainTimer.Tick += _mainTimer_Tick;
            _mainTimer.Start();
        }

        private void _mainTimer_Tick(object sender, EventArgs e)
        {
            MainLabel.Content += $"hello ::  { i } \n" ;
            i++;

            if (!tcpClient.Connected)
            {
                Reconnect();
            }

            if (tcpClient.Available > 0 || twitchReader.Peek() >= 0)
            {
                var receivedText = twitchReader.ReadLine();
                ChatTextParser(receivedText);
            }
        }

        private void ChatTextParser(string receivedText)
        {
            MainLabel.Content += receivedText + Environment.NewLine;
        }

        public void InitializeBOT()
        {
            tcpClient = new TcpClient("irc.chat.twitch.tv", 6667);
            twitchReader = new StreamReader(tcpClient.GetStream());
            twitchWriter = new StreamWriter(tcpClient.GetStream());
        }

        public void JoinChatRoom()
        {
            chatRoomName = ChatRoomTextBox.Text;
            twitchWriter.WriteLine(String.Format("PASS {0}\r\nNICK {1}\r\nUser {1} 8 * :{1}", botPassword, botUserName));
            //twitchWriter.WriteLine("CAP REQ :twitch.tv/membership");
            twitchWriter.WriteLine("JOIN #" + chatRoomName);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SetupAndStartTimer();
        }

        public void Reconnect()
        {
            if(chatRoomName != string.Empty)
            {
                JoinChatRoom();
            }
        }

        public void SendMessage(string message)
        {
            twitchWriter.WriteLine(chatCommand + " " + message);
        }

        public void ReceiveMessage(string speaker, string receivedMessage)
        {

        }

    }
}
